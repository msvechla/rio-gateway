package gateway

import (
	"net/http"

	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"gitlab.com/msvechla/rio-gateway/internal/pkg/communication/rabbitmq"
	"gitlab.com/msvechla/rio-gateway/internal/pkg/communication/socket"
)

// Gateway stores all necessary information about the gateway
type Gateway struct {
	ID                       string
	Webserver                *socket.Server
	OutgoingAMQPMessage      chan *messaging.Container
	OutgoingWebsocketMessage chan *messaging.Container
	BindToGameID             chan *rabbitmq.SyncBind
	UnbindFromGameID         chan string
	AMQPClient               *rabbitmq.Client
}

// NewGateway returns a new gateway object
func NewGateway(id string, amqpURL string, amqpUser string, amqpPass string, sessionsURL string) (*Gateway, error) {
	var outgoingAMQPMessage = make(chan *messaging.Container)
	var messageToUser = make(chan *messaging.Container)
	var bindToGameID = make(chan *rabbitmq.SyncBind)
	var unbindFromGameID = make(chan string)

	ws := socket.NewServer(outgoingAMQPMessage, bindToGameID, unbindFromGameID, sessionsURL)

	gw := Gateway{
		ID:                       id,
		Webserver:                ws,
		OutgoingAMQPMessage:      outgoingAMQPMessage,
		OutgoingWebsocketMessage: messageToUser,
		BindToGameID:             bindToGameID,
		UnbindFromGameID:         unbindFromGameID,
	}

	amqpClient, err := rabbitmq.NewClient(&gw.ID, amqpURL, amqpUser, amqpPass)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating AMQP client")
	}
	log.Info("Successfully established AMQP connection")
	gw.AMQPClient = amqpClient

	return &gw, nil
}

// Run executes all go routines for handling asynchronous tasks
func (g *Gateway) Run(webserverHost string) {
	go g.handleOutgoingMessages()
	go g.broadcastAMQPMessagesToUsers()
	g.Webserver.Bootstrap()
	http.ListenAndServe(webserverHost, g.Webserver)
}

// handleOutgoingMessages listens on the OutgoingAMQPMessages channel and publishes the messages to the exchange
func (g *Gateway) handleOutgoingMessages() {
	log.Info("Handling incoming messages from users")
	for {
		select {
		case m := <-g.OutgoingAMQPMessage:
			log.Infof("New message from user %s: %+v", m.UserID, m.Msg)

			m.GatewayID = g.ID

			err := g.AMQPClient.PublishMsgFromGateway(m)
			if err != nil {
				log.Errorf("Error publishing message to AMQP: %s", err)
			}
		case bindID := <-g.BindToGameID:
			g.AMQPClient.BindQueueToGameID(bindID)
		case unbindID := <-g.UnbindFromGameID:
			// if no more connections exists for a specific game, unbind the queue
			if !g.Webserver.ConnectionForGameExists(unbindID) {
				g.AMQPClient.UnbindQueueFromGameID(unbindID)
			}
		}
	}
}

// broadcastAMQPMessagesToUsers listens for incoming amqp messages and sends them to the all clients connected to a game
func (g *Gateway) broadcastAMQPMessagesToUsers() {
	log.Info("Handling incoming AMQP messages to users from gameserver")
	msgs, err := g.AMQPClient.Incoming.Consume(
		*g.AMQPClient.ID, // queue
		"",               // consumer
		true,             // auto ack
		true,             // exclusive
		false,            // no local
		false,            // no wait
		nil,              // args
	)

	if err != nil {
		log.Errorf("Error consuming from queue %s: %s", *g.AMQPClient.ID, err)
	}

	for {
		select {
		case m := <-msgs:
			var msg messaging.Container
			err := proto.Unmarshal(m.Body, &msg)

			if err != nil {
				log.Errorf("Error unmarshaling protobuf message: %s, message was: %s", err, m.Body)
				continue
			}
			log.Printf("New message for game %s: %+v", msg.GameID, msg)

			switch msg.Msg.(type) {

			case *messaging.Container_KickUser:

				conn, err := g.Webserver.GetConnectionForUserInGame(msg.GetKickUser().GetUserID(), msg.GameID)
				if err != nil {
					log.Errorf("Error getting connection for user to kick: %s", err)
					continue
				}

				conn.Close("kicked by host")

			default:
				// broadcast message to all users in the game
				conns, err := g.Webserver.GetConnectionsForGame(msg.GameID)
				if err != nil {
					log.Errorf("Error getting connections for game: %s", err)
					continue
				}

				for _, c := range conns {
					err = c.SendMessage(&msg)
					if err != nil {
						log.Errorf("Error sending message to user via websocket: %s", err)
					}
				}
			}
		}
	}
}
