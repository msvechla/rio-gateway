module gitlab.com/msvechla/rio-gateway

go 1.13

require (
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0
	github.com/pkg/errors v0.9.1
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/streadway/amqp v1.0.0
	github.com/urfave/cli/v2 v2.11.2
	gitlab.com/msvechla/rio-gameserver v0.0.0
	golang.org/x/sys v0.0.0-20220825204002-c680a09ffe64 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)

replace gitlab.com/msvechla/rio-gameserver => ../rio-gameserver
