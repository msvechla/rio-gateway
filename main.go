package main

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/msvechla/rio-gateway/internal/pkg/gateway"
)

var (
	flagAMQPURL     string
	flagAMQPUser    string
	flagAMQPPass    string
	flagSessionsURL string
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})

	app := initCLI()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

	hostname, err := os.Hostname()

	if err != nil {
		log.Fatalf("Error determining hostname: %s", err)
	}

	gw, err := gateway.NewGateway(
		fmt.Sprintf("gw-%s", hostname),
		flagAMQPURL,
		flagAMQPUser,
		flagAMQPPass,
		flagSessionsURL,
	)
	if err != nil {
		log.Fatal(err)
	}

	gw.Run("0.0.0.0:44444")
}

// initCLI initializes the command line
func initCLI() *cli.App {
	app := &cli.App{
		Name:    "rio-gateway",
		Usage:   "",
		Version: "v2.1.1",
	}

	generalFlags := []cli.Flag{
		&cli.StringFlag{
			Name:        "amqpURL",
			Value:       "localhost:5672/",
			Usage:       "AMQP URL to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_URL"},
			Destination: &flagAMQPURL,
		},
		&cli.StringFlag{
			Name:        "amqpUser",
			Value:       "user",
			Usage:       "AMQP user to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_USER"},
			Destination: &flagAMQPUser,
		},
		&cli.StringFlag{
			Name:        "amqpPass",
			Value:       "helloworld",
			Usage:       "AMQP Password to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_PASS"},
			Destination: &flagAMQPPass,
		},
		&cli.StringFlag{
			Name:        "sessionsURL",
			Value:       "http://localhost:11111/sessions",
			Usage:       "Endpoint of the gio-sessions service.",
			EnvVars:     []string{"SESSIONS_URL"},
			Destination: &flagSessionsURL,
		},
	}

	app.Flags = append(app.Flags, generalFlags...)

	return app
}
