package socket

import (
	"fmt"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
)

// Connection represents a clients websocket connection to the gateway
type Connection struct {
	UserID    string
	GameID    string
	State     messaging.Connection_State
	Websocket *websocket.Conn
	Server    *Server
	Log       *log.Entry
}

// NewConnection returns a new connection object
func NewConnection(userID string, gameID string, websocket *websocket.Conn, server *Server) *Connection {
	c := Connection{
		UserID:    userID,
		GameID:    gameID,
		Websocket: websocket,
		Server:    server,
		State:     messaging.Connection_OPEN,
	}

	c.Log = log.WithFields(log.Fields{
		"UserID": c.UserID,
		"gameID": c.GameID,
	})
	c.Log.Info("Created new Connection")

	return &c
}

// SendMessage sends a message to the user via the underlying websocket
func (c *Connection) SendMessage(msg *messaging.Container) error {
	bytes, err := proto.Marshal(msg)
	if err != nil {
		return errors.Wrap(err, "marshaling protobuf message")
	}

	return c.Websocket.WriteMessage(websocket.BinaryMessage, bytes)
}

// NewMessage creates a new message by ensureing the metadata is not exploited / overwritten by the client
func (c *Connection) NewMessage(msg *messaging.Container) *messaging.Container {
	return &messaging.Container{UserID: c.UserID, GameID: c.GameID, Msg: msg.Msg}
}

// Listen for incoming messages from the client via websocket, process them and hand them over to the outgoing messages channel
func (c *Connection) Listen() {
	for {
		select {
		default:
			_, bytes, err := c.Websocket.ReadMessage()
			if err != nil {
				log.Errorf("Received error from user %s: %s", c.UserID, err)
				c.Close(err.Error())
				return
			}

			var msg messaging.Container
			err = proto.Unmarshal(bytes, &msg)
			if err != nil {
				log.Errorf("Received error from user %s: %s", c.UserID, err)
				c.Close(err.Error())
				return
			}

			// handle the message based on its type
			switch msg.Msg.(type) {

			case *messaging.Container_Connection:
				switch msg.GetConnection().GetState() {
				case messaging.Connection_INIT:
					c.State = messaging.Connection_INIT
				case messaging.Connection_DISCONNECTED:
					c.State = messaging.Connection_DISCONNECTED
					c.Close("user disonnected")
					return
				default:
					errMsg := fmt.Sprintf("Invalid ConnectionState from user %s: %s", c.UserID, msg.GetConnection().GetState())
					_ = c.Websocket.WriteMessage(websocket.TextMessage, []byte(errMsg))
					log.Error(errMsg)
					c.Close(errMsg)
					return
				}
			}

			if !c.isValidMessageType(msg) {
				errMsg := fmt.Sprintf("Received invalid message type %+v for userID %s", msg.Msg, c.UserID)
				_ = c.Websocket.WriteMessage(websocket.TextMessage, []byte(errMsg))
				log.Error(errMsg)
				c.Close(errMsg)
			}

			safeMsg := c.NewMessage(&msg)
			c.Server.OutgoingAMQPMessage <- safeMsg
		}
	}
}

// Close closes a connection and notifies the gameserver when necessary
func (c *Connection) Close(reason string) {
	log.Infof("Closing connection of user %s with reason: %s", c.UserID, reason)

	c.Websocket.Close()
	c.Server.handleDisconnect(c.UserID, c.GameID)
	c.Server.UnbindFromGameID <- c.GameID

	if c.State == messaging.Connection_OPEN {
		return
	}

	// notify the gameserver that the user disconnected
	// TODO: remove code entirely if no more needed

	// msg := messaging.TypeMeta{
	// 	UserID: c.UserID,
	// 	GameID: c.GameID,
	// 	Type:   messaging.MsgTypeConnection,
	// 	Body: messaging.TypeConnectionBody{
	// 		State: messaging.MsgTypeConnectionStateDisconnected,
	// 	},
	// }

	// c.Server.OutgoingAMQPMessage <- &msg
}

// isValidMessageType checks whether the message type is valid for the user or host
func (c *Connection) isValidMessageType(msg messaging.Container) bool {
	// if the connection did not originate from the host, ensure msg type is not host only
	if c.UserID != messaging.SpecialUserID_HOST.String() {
		switch msg.Msg.(type) {
		case *messaging.Container_HostUpdate:
			return false
		case *messaging.Container_KickUser:
			return false
		}
	}

	return true
}
