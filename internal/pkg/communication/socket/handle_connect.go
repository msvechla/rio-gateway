package socket

import (
	"bytes"
	"fmt"
	"net/http"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"gitlab.com/msvechla/rio-gateway/internal/pkg/communication/rabbitmq"
)

// handleConnect handles an incoming websocket connection of a player for a specific game
func (s *Server) handleConnect(responseWriter http.ResponseWriter, request *http.Request) {
	gameServerID := getGameServerIDFromURL(request.URL)

	var playerName string

	playerName = getUsernameFromURL(request.URL)

	// generate a unique player name if it is a request from a host
	if playerName == "" {
		playerName = messaging.SpecialUserID_HOST.String()
	}

	log.Info(request.Cookies())

	// upgrade to websocket connection
	socket, err := upgrader.Upgrade(responseWriter, request, nil)
	if err != nil {
		log.Errorf("Error upgrading websocket connection: %s, request: %v", err, request)
		socket.Close()
	}

	// check cookie after upgrading the connection, so we can close with a custom websocket error code that we can react on the frontend
	cookie, err := request.Cookie(authCookieName(gameServerID, playerName))
	if err != nil {
		log.Errorf("Error retrieving cookie for request: %s", err)

		err := socket.WriteMessage(
			websocket.CloseMessage,
			websocket.FormatCloseMessage(4001, "Access Denied - error retrieving session cookie"),
		)
		if err != nil {
			log.Errorf("Error closing websocket connection: %s", err)
		}
		socket.Close()
		return
	}

	// verify token
	validToken, err := s.verifyToken(gameServerID, playerName, cookie.Value)
	if err != nil {
		log.Errorf("Error verifying token: %s", err)

		err := socket.WriteMessage(
			websocket.CloseMessage,
			websocket.FormatCloseMessage(websocket.CloseInternalServerErr, "Internal Server Error"),
		)
		if err != nil {
			log.Errorf("Error closing websocket connection: %s", err)
		}
		socket.Close()
		return
	}

	if !validToken {
		log.Errorf("Retrieved invalid token for user %s in gameID %s", playerName, gameServerID)

		err := socket.WriteMessage(
			websocket.CloseMessage,
			websocket.FormatCloseMessage(4000, "Access Denied - you might have been kicked by the host"),
		)
		if err != nil {
			log.Errorf("Error closing websocket connection: %s", err)
		}
		socket.Close()
		return
	}

	// create connection and add to connections
	c := NewConnection(playerName, gameServerID, socket, s)
	s.Connection[fmt.Sprintf("%s/%s", gameServerID, playerName)] = c

	if _, exists := s.ConnectionList[gameServerID]; !exists {
		s.ConnectionList[gameServerID] = []*Connection{}
	}
	s.ConnectionList[gameServerID] = append(s.ConnectionList[gameServerID], c)

	// bind to the gameID
	done := make(chan struct{})
	c.Server.BindToGameID <- &rabbitmq.SyncBind{GameID: gameServerID, Done: &done}
	<-done

	go c.Listen()
}

// verifyToken verifies a user token by queriying the sessions service
func (s *Server) verifyToken(gameID string, userID string, token string) (bool, error) {
	msg := messaging.Container{
		Msg: &messaging.Container_UserToken{
			UserToken: &messaging.UserToken{
				UserID: userID,
				Token:  token,
			},
		},
	}
	b, err := proto.Marshal(&msg)
	if err != nil {
		return false, errors.Wrap(err, "marshaling user token request")
	}

	resp, err := http.Post(
		fmt.Sprintf("%s/internal/token/verify/%s", s.sessionsURL, gameID),
		"application/json",
		bytes.NewReader(b),
	)
	if err != nil {
		return false, err
	}

	if resp.StatusCode != http.StatusOK {
		return false, nil
	}

	return true, nil
}

func authCookieName(gameServerID string, userID string) string {
	return fmt.Sprintf("refinable.io-%s-%s", gameServerID, userID)
}
