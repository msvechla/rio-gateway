{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "rio-gateway.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "rio-gateway.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "rio-gateway.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "rio-gateway.labels" -}}
helm.sh/chart: {{ include "rio-gateway.chart" . }}
{{ include "rio-gateway.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "rio-gateway.selectorLabels" -}}
app.kubernetes.io/name: {{ include "rio-gateway.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Common Annotations
*/}}
{{- define "rio-gateway.annotations" -}}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- if .Values.prometheus.enabled }}
{{ include "rio-gateway.prometheusAnnotations" . }}
{{- end }}
{{- if .Values.linkerd.enabled }}
linkerd.io/inject: enabled
{{- end }}
{{- end -}}

{{/*
Prometheus Annotations
*/}}
{{- define "rio-gateway.prometheusAnnotations" -}}
prometheus.io/path: /metrics
prometheus.io/port: "9102"
prometheus.io/scrape: "true"
{{- end -}}


{{/*
Create the name of the service account to use
*/}}
{{- define "rio-gateway.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "rio-gateway.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}
