package socket

import (
	"net/http"
	"net/url"
	"path"
)

// routes establishes all routes for the Server
func (s *Server) routes() {

	// /api/connect/123?playerName=wasd
	s.router.HandleFunc("/gateway/connect/{gameID}", s.handleConnect)
	s.router.HandleFunc("/health", s.handleHealth)
}

// handleHealth handles an incoming health check
func (s *Server) handleHealth(responseWriter http.ResponseWriter, request *http.Request) {
	// TODO: add healthcheck logic
	responseWriter.WriteHeader(http.StatusOK)
}

// getGameServerIDFromURL parses the gameserver id from the connection url
func getGameServerIDFromURL(requestURL *url.URL) string {
	return path.Base(requestURL.Path)
}

// getUsernameFromURL parses the username from the connection url
func getUsernameFromURL(requestURL *url.URL) string {
	return requestURL.Query().Get(queryKeyUsername)
}
