package socket

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"gitlab.com/msvechla/rio-gateway/internal/pkg/communication/rabbitmq"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

const (
	queryKeyUsername = "playerName"
	queryKeyHost     = "host"
	actionJoinGame   = "join"
	actionHostGame   = "host"
)

// Server listens for incoming requests and handles new connections and outgoing messages to amqp
type Server struct {
	// Connection contains the websocket of a gameID/userID
	Connection          map[string]*Connection
	ConnectionList      map[string][]*Connection
	OutgoingAMQPMessage chan *messaging.Container
	BindToGameID        chan *rabbitmq.SyncBind
	UnbindFromGameID    chan string
	router              *mux.Router
	sessionsURL         string
}

// NewServer returns a Server object
func NewServer(outgoingAMQPMessage chan *messaging.Container, bindToGameID chan *rabbitmq.SyncBind, unbindFromGameID chan string, sessionsURL string) *Server {
	return &Server{
		Connection:          make(map[string]*Connection),
		ConnectionList:      make(map[string][]*Connection),
		OutgoingAMQPMessage: outgoingAMQPMessage,
		BindToGameID:        bindToGameID,
		UnbindFromGameID:    unbindFromGameID,
		router:              mux.NewRouter(),
		sessionsURL:         sessionsURL,
	}
}

// Bootstrap initializes the routes on the Server and listens for connections
func (s *Server) Bootstrap() {
	log.Info("Bootstrapping Server")

	// TODO: remove me for prod
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	s.routes()
}

// ServeHTTP satisfies the http handler interface
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

// GetConnectionForUserInGame returns the connection for a user of a specific game
func (s *Server) GetConnectionForUserInGame(userID string, gameID string) (*Connection, error) {
	conn, exists := s.Connection[fmt.Sprintf("%s/%s", gameID, userID)]
	if !exists {
		return nil, fmt.Errorf("UserID %s not found for gameID %s", userID, gameID)
	}
	return conn, nil
}

// GetConnectionsForGame returns all connections for a specific game
func (s *Server) GetConnectionsForGame(gameID string) ([]*Connection, error) {
	conns, exists := s.ConnectionList[gameID]
	if !exists {
		return nil, fmt.Errorf("GameID %s not found", gameID)
	}
	return conns, nil
}

// ConnectionForGameExists determines whether a connection for a game still exists
func (s *Server) ConnectionForGameExists(gameID string) bool {
	_, exists := s.ConnectionList[gameID]
	return exists
}

// handleDisconnect handles user disconnecting from the gateway
func (s *Server) handleDisconnect(userID string, gameID string) {
	for i, c := range s.ConnectionList[gameID] {
		if c.UserID == userID {
			s.ConnectionList[gameID] = append(s.ConnectionList[gameID][:i], s.ConnectionList[gameID][i+1:]...)
			break
		}
	}
	delete(s.Connection, fmt.Sprintf("%s/%s", gameID, userID))
}
