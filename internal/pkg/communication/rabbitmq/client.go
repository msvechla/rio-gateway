package rabbitmq

import (
	"fmt"
	"net/url"

	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
)

const (
	// ExchangeMsgFromGateway is the exchange for publishing messages from the gateway
	ExchangeMsgFromGateway = "MsgFromGateway"
	// ExchangeBroadcastMsgToGateway is the exchange where the gameservers publish messages for the gateway
	ExchangeBroadcastMsgToGateway = "BroadcastMsgToGateway"
)

// Client used to communicate with rabbitmq
type Client struct {
	ID       *string
	AMQPUrl  string
	Conn     *amqp.Connection
	Outgoing *amqp.Channel
	Incoming *amqp.Channel
}

// SyncBind is send on a channel to synchronize a bind operation
type SyncBind struct {
	GameID string
	Done   *chan struct{}
}

// NewClient returns the amqp client
func NewClient(id *string, amqpURL string, amqpUser string, amqpPass string) (*Client, error) {

	url := fmt.Sprintf("amqp://%s:%s@%s", url.PathEscape(amqpUser), url.PathEscape(amqpPass), amqpURL)

	c := Client{ID: id, AMQPUrl: url}
	return &c, c.Init()
}

// Init initializes the amqp client
func (c *Client) Init() error {
	var err error
	c.Conn, err = amqp.Dial(c.AMQPUrl)

	if err != nil {
		return errors.Wrap(err, "Error establishing AMQP connection")
	}

	// TODO: implememnt correct reconnect logic for rabbitmq
	go func() {
		log.Fatalf("RabbitMQ closed connection, restarting to reconnect: %s", <-c.Conn.NotifyClose(make(chan *amqp.Error)))
	}()

	c.Outgoing, err = c.NewChannel()
	if err != nil {
		return errors.Wrapf(err, "Error establishing AMQP channel Outgoing")
	}

	c.Incoming, err = c.NewChannel()
	if err != nil {
		return errors.Wrapf(err, "Error establishing AMQP channel Incoming")
	}

	err = c.Outgoing.ExchangeDeclare(
		ExchangeMsgFromGateway, // name
		"direct",               // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrapf(err, "Error declaring exchange %s", ExchangeMsgFromGateway)
	}

	err = c.Outgoing.ExchangeDeclare(
		ExchangeBroadcastMsgToGateway, // name
		"direct",                      // type
		true,                          // durable
		false,                         // auto-deleted
		false,                         // internal
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return errors.Wrapf(err, "Error declaring exchange %s", ExchangeBroadcastMsgToGateway)
	}

	err = c.EnsureGatewayQueueExists()
	if err != nil {
		return errors.Wrapf(err, "Error setting up gateway queue %s", err)
	}

	return nil
}

// EnsureGatewayQueueExists declares a queue for the gateway that receives messages from the gameservers, bound to the MsgToGateway Exchange.
// Gameservers will have to route messages via the key of the gateway
func (c *Client) EnsureGatewayQueueExists() error {

	q, err := c.Incoming.QueueDeclare(
		*c.ID, // name
		true,  // durable
		true,  // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)

	if err != nil {
		return errors.Wrapf(err, "Error declaring queue for gateway %s", *c.ID)
	}
	log.Infof("Successfully declared queue for gateway %s", q.Name)

	err = c.Incoming.QueueBind(
		q.Name,                        // queue name
		*c.ID,                         // routing key
		ExchangeBroadcastMsgToGateway, // exchange
		false,
		nil)

	if err != nil {
		return errors.Wrapf(err, "Error binding queue %s to exchange %s", q.Name, ExchangeBroadcastMsgToGateway)
	}

	log.Infof("Successfully bound queue %s to exchange %s via %s", q.Name, ExchangeBroadcastMsgToGateway, *c.ID)
	return nil
}

// BindQueueToGameID binds the gateways queue for incoming messages for a specific gameID
func (c *Client) BindQueueToGameID(syncBind *SyncBind) error {

	err := c.Incoming.QueueBind(
		*c.ID,                         // queue name
		syncBind.GameID,               // routing key
		ExchangeBroadcastMsgToGateway, // exchange
		false,
		nil)

	*syncBind.Done <- struct{}{}

	if err != nil {
		return errors.Wrapf(err, "Error binding queue %s to exchange %s via routing key for gameID %s", *c.ID, ExchangeMsgFromGateway, syncBind.GameID)
	}

	log.Infof("Successfully bound queue %s to exchange %s via %s", syncBind.GameID, ExchangeMsgFromGateway, syncBind.GameID)
	return nil
}

// UnbindQueueFromGameID unbinds the gateways queue from incoming messages for a specific gameID
func (c *Client) UnbindQueueFromGameID(gameID string) error {

	err := c.Incoming.QueueUnbind(
		*c.ID,                         // queue name
		gameID,                        // routing key
		ExchangeBroadcastMsgToGateway, // exchange
		nil)

	if err != nil {
		return errors.Wrapf(err, "Error unbinding queue %s from exchange %s via routing key for gameID %s", *c.ID, ExchangeMsgFromGateway, gameID)
	}

	log.Infof("Successfully unbound queue %s from exchange %s via %s", gameID, ExchangeMsgFromGateway, gameID)
	return nil
}

// NewChannel returns a new channel from the amqp connection
func (c *Client) NewChannel() (*amqp.Channel, error) {
	ch, err := c.Conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "Error opening AMQP channel")
	}
	return ch, nil
}

// PublishMsgFromGateway publishes a message from the gateway to the gameservers
func (c *Client) PublishMsgFromGateway(message *messaging.Container) error {
	body, err := proto.Marshal(message)
	if err != nil {
		return errors.Wrapf(err, "Error marshaling protobuf message: %v", message)
	}

	log.Infof("Original message: %v", message)
	log.Infof("Published message: %s", body)
	c.Outgoing.Publish(
		ExchangeMsgFromGateway, // exchange
		message.GameID,         // routing key
		false,                  // mandatory
		false,                  // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body,
			AppId:       *c.ID,
		})
	return nil
}
