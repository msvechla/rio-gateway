# Refinable.io Gateway

[![go report](https://goreportcard.com/badge/gitlab.com/msvechla/rio-gateway)](https://goreportcard.com/report/gitlab.com/msvechla/rio-gateway)

*The user-facing gateway for [refinable.io](https://refinable.io) - written in golang.*

**This microservice is part of the refinable.io stack:**

- [rio-frontend](https://gitlab.com/msvechla/rio-frontend) - the vuejs frontend
- [rio-gateway](https://gitlab.com/msvechla/rio-gateway) - the main user-facing gateway
- [rio-session](https://gitlab.com/msvechla/rio-sessions) - microservice for issuing session tokes and creating games
- [rio-gameserver](https://gitlab.com/msvechla/rio-gameserver) - service hosting the main refining session logic

An overview of the microservice architecture can be found [here](https://gitlab.com/msvechla/rio-gameserver/-/blob/master/docs/architecture.md).

## Table of Contents

<!-- TOC -->

- [Table of Contents](#table-of-contents)
- [Capabilities](#capabilities)
- [Build](#build)
- [Deployment](#deployment)
- [Contributing](#contributing)
- [Versioning](#versioning)
- [Authors](#authors)
- [License](#license)

<!-- /TOC -->

## Capabilities

- handles client websocket connections
- validates client authentication against sessions service
- asynchronous messaging between clients and their gameserver via rabbitmq
- routing of messages to correct client / gameserver

## Build

Take a look at the supplied [Dockerfile](Dockerfile) for more information on the build process.

## Deployment

The sessions service is deployed to Kubernetes via the included Helm chart. See [k8s/rio-gateway](k8s/rio-gateway) for details.

## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/msvechla/rio-gateay/tags) or take a look at the [CHANGELOG.md](./CHANGELOG.md)

## Authors

- **Marius Svechla** - *Initial work*

See also the list of [contributors](https://gitlab.com/msvechla/rio-gateway/graphs/master) who participated in this project.

## License

[MIT License](./License.md)

Copyright (c) [2020] [Marius Svechla]
